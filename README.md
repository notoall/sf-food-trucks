# README #

This application parses data from [SF Food trucks](https://data.sfgov.org/Economy-and-Community/Mobile-Food-Facility-Permit/rqzj-sfat?) and populates a map. It also provides the food available in the area and provides filtering on the foods that are desired. If a marker is clicked it will display the facility's information. The backend of the server uses a GET request with the 4 bounds xmin, ymin, xmax, ymax of the map (basically the diagonal) and retrieves the appropriate dataset from the Food Trucks api.

### Prerequisites ###
* Java
* Maven
* Tomcat

Just run mvn clean install in your project root directory and it should build a war than you can deploy onto your tomcat server

**Live Demo**: [Link](http://168.235.144.16:8080/FoodTruckService/)
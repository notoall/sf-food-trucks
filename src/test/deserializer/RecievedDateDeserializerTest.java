package deserializer;

import com.fasterxml.jackson.core.JsonParser;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Calendar;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * Created by Joshua on 1/20/2015.
 */
public class RecievedDateDeserializerTest {
    RecievedDateDeserializer deserializer;

    @Mock
    private JsonParser jsonParser;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        deserializer = new RecievedDateDeserializer();
    }

    @Test(expected = RuntimeException.class)
    public void testDeserializeFail() throws Exception {
        Date date = Calendar.getInstance().getTime();
        when(jsonParser.getText()).thenReturn(date.toString());
        deserializer.deserialize(jsonParser, null);
    }

    @Test
    public void testDeserializeSuccess() throws Exception {
        when(jsonParser.getText()).thenReturn("Mar 14 2014  1:05PM");
        Date deserialized = deserializer.deserialize(jsonParser, null);
        assertEquals(deserialized.getTime(), 1394784300000l);
    }
}

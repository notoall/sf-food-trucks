package deserializer;

import com.fasterxml.jackson.core.JsonParser;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static org.junit.Assert.assertNull;
/**
 * Created by Joshua on 1/20/2015.
 */
public class FoodItemsDeserializerTest {
    FoodItemsDeserializer deserializer;

    @Mock
    private JsonParser jsonParser;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        deserializer = new FoodItemsDeserializer();
    }

    @Test
    public void testNull() throws Exception {
        when(jsonParser.getText()).thenReturn(null);
        assertNull(deserializer.deserialize(jsonParser, null));
    }

    @Test
    public void testEmpty() throws Exception {
        when(jsonParser.getText()).thenReturn("");
        assertNull(deserializer.deserialize(jsonParser,null));
    }

    @Test
    public void testValid() throws Exception {
        when(jsonParser.getText()).thenReturn("Cold Truck: Sandwiches: fruit: snacks: candy: hot and cold drinks");
        Set<String> foodItems = deserializer.deserialize(jsonParser,null);
        assertEquals(foodItems.size(),6);
    }
	
	@Test
	public void testFail() throws Exception {
		assertNull(new Integer(1));
	}
	
}

var truckImage = 'images/truck.png';
var cartImage = 'images/cart.png';

var map;
var markers = {};
function initialize() {
    var mapOptions = {
        zoom: 15,
        center: new google.maps.LatLng(37.7933225227924, -122.440119274464)
    };
    map = new google.maps.Map(document.getElementById('map-canvas'),
        mapOptions);

    google.maps.event.addListener(map, 'idle', function () {
        $('#restaurant-info').hide();
        var bounds = map.getBounds();
        setMap(markers,null, true);
        populateMap(bounds);
    });
}

function setMap(object, value, del){
    for (var objectId in object){
        if (object.hasOwnProperty(objectId)) {
            object[objectId].setMap(value);
            if(del) delete object[objectId];
        }
    }
}


function showFoodItems(foodAvailable){
    var items = '';
    for (var food in foodAvailable){
        if (foodAvailable.hasOwnProperty(food)) items+='<option value="'+food+'">'+food+'</option>';
    }
    setMap(markers,map,false);
    $('#food-items').html(items);
}

function populateMap(bounds) {
    var ne = bounds.getNorthEast(); // LatLng of the north-east corner
    var sw = bounds.getSouthWest(); // LatLng of the south-west corder

    var apiUrl = 'rest/facilities/';

    $.getJSON(apiUrl,{
        xmin : ne.lat(),
        ymin: sw.lng(),
        xmax: sw.lat(),
        ymax: ne.lng()
    }).done(function (data) {
        var foodAvailable = {};

        for (var i = 0; i < data.length; i++) {
            if (data[i].location != null && data[i].applicant != null && data[i].objectid && markers[data[i].objectid] === undefined) {
                var loc = new google.maps.LatLng(data[i].location.latitude, data[i].location.longitude);


                if(data[i].fooditems){
                    var foods = data[i].fooditems;
                    for(var j = 0 ; j < foods.length; j++){
                        if(foodAvailable[foods[j]]){
                            foodAvailable[foods[j]].push(data[i].objectid);
                        } else {
                            foodAvailable[foods[j]] = [data[i].objectid];
                        }
                    }
                }

                var marker = new google.maps.Marker({
                    position: loc,
                    map: map,
                    icon: (data[i].facilitytype === 'Truck') ? truckImage : cartImage,
                    title: data[i].applicant,
                    data: data[i]
                });

                markers[data[i].objectid]=marker;

                attachListener(marker, data[i].objectid);
            }
        }
        showFoodItems(foodAvailable);
    });

}

function attachListener(marker, id){
    google.maps.event.addListener(marker, 'click', function () {
        //map.panTo(marker.getPosition());
        $('#facility-name').html(marker.data.applicant);
        $('#facility-address').html(marker.data.address);
        $('#facility-type').html(marker.data.facilitytype);
        $('#facility-permit').html(marker.data.permit);
        $('#facility-permit-status').html(marker.data.status);
        $('#facility-foods').html(marker.data.fooditems.join("<br />"));
        $('#facility-description').html(marker.data.locationdescription);
        $('#restaurant-info').show();
    });
}


$( "#food-items" ).change(function () {
    setMap(markers,null,false);
    if( $( "#food-items option:selected").length>0) {
        $("#food-items option:selected").each(function () {
            for (var objectId in markers) {
                if (markers.hasOwnProperty(objectId) && $.inArray($(this).text(), markers[objectId].data.fooditems)!=-1) {
                    markers[objectId].setMap(map);
                }
            }
        });
    }
});

google.maps.event.addDomListener(window, 'load', initialize);
package model;

/**
 * Created by Joshua on 1/19/2015.
 */
public class Location {
    private boolean needs_recoding;
    private float latitude;
    private float longitude;

    public boolean isNeeds_recoding() {
        return needs_recoding;
    }

    public void setNeeds_recoding(boolean needs_recoding) {
        this.needs_recoding = needs_recoding;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    @Override
    public String toString() {
        return "Location{" +
                "needs_recoding=" + needs_recoding +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                '}';
    }
}

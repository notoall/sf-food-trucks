package model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import deserializer.FoodItemsDeserializer;
import deserializer.RecievedDateDeserializer;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;
import java.util.Set;

/**
 * Created by Joshua on 1/12/2015.
 * <p/>
 * JSON input in the format:
 * {
 * "location" : {
 * "needs_recoding" : false,
 * "longitude" : "-122.398658184594",
 * "latitude" : "37.7901490874965"
 * },
 * "status" : "APPROVED",
 * "expirationdate" : "2015-03-15T00:00:00",
 * "permit" : "14MFF-0102",
 * "block" : "3708",
 * "received" : "Jun  2 2014 12:23PM",
 * "facilitytype" : "Truck",
 * "blocklot" : "3708055",
 * "locationdescription" : "01ST ST: STEVENSON ST to JESSIE ST (21 - 56)",
 * "cnn" : "101000",
 * "priorpermit" : "0",
 * "approved" : "2014-06-02T15:32:00",
 * "schedule" : "http://bsm.sfdpw.org/PermitsTracker/reports/report.aspx?title=schedule&report=rptSchedule&params=permit=14MFF-0102&ExportPDF=1&Filename=14MFF-0102_schedule.pdf",
 * "address" : "50 01ST ST",
 * "applicant" : "Cupkates Bakery, LLC",
 * "lot" : "055",
 * "fooditems" : "Cupcakes",
 * "longitude" : "-122.398658184604",
 * "latitude" : "37.7901490737255",
 * "objectid" : "546631",
 * "y" : "2115738.283",
 * "x" : "6013063.33"
 * }
 */

@XmlRootElement
public class MobileFoodFacility {

    private int cnn;
    private int objectid;
    private float x;
    private float y;

    private float latitude;
    private float longitude;

    private String status;
    private String permit;
    private String block;
    private String facilitytype;
    private String blocklot;
    private String locationdescription;
    private String priorpermit;
    private String schedule;
    private String address;
    private String applicant;
    private String lot;

    @JsonDeserialize(using = RecievedDateDeserializer.class)
    private Date received;
    private Date noisent;
    private Date expirationdate;
    private Date approved;

    private Location location;

    @JsonDeserialize(using = FoodItemsDeserializer.class)
    private Set<String> fooditems;

    public int getCnn() {
        return cnn;
    }

    public void setCnn(int cnn) {
        this.cnn = cnn;
    }

    public int getObjectid() {
        return objectid;
    }

    public void setObjectid(int objectid) {
        this.objectid = objectid;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPermit() {
        return permit;
    }

    public void setPermit(String permit) {
        this.permit = permit;
    }

    public String getBlock() {
        return block;
    }

    public void setBlock(String block) {
        this.block = block;
    }

    public String getFacilitytype() {
        return facilitytype;
    }

    public void setFacilitytype(String facilitytype) {
        this.facilitytype = facilitytype;
    }

    public String getBlocklot() {
        return blocklot;
    }

    public void setBlocklot(String blocklot) {
        this.blocklot = blocklot;
    }

    public String getLocationdescription() {
        return locationdescription;
    }

    public void setLocationdescription(String locationdescription) {
        this.locationdescription = locationdescription;
    }

    public String getPriorpermit() {
        return priorpermit;
    }

    public void setPriorpermit(String priorpermit) {
        this.priorpermit = priorpermit;
    }

    public String getSchedule() {
        return schedule;
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getApplicant() {
        return applicant;
    }

    public void setApplicant(String applicant) {
        this.applicant = applicant;
    }

    public String getLot() {
        return lot;
    }

    public void setLot(String lot) {
        this.lot = lot;
    }

    public Date getReceived() {
        return received;
    }

    public void setReceived(Date received) {
        this.received = received;
    }

    public Date getNoisent() {
        return noisent;
    }

    public void setNoisent(Date noisent) {
        this.noisent = noisent;
    }

    public Date getExpirationdate() {
        return expirationdate;
    }

    public void setExpirationdate(Date expirationdate) {
        this.expirationdate = expirationdate;
    }

    public Date getApproved() {
        return approved;
    }

    public void setApproved(Date approved) {
        this.approved = approved;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Set<String> getFooditems(){
        return fooditems;
    }

    public void setFooditems(Set<String> fooditems) { this.fooditems = fooditems; }

    @Override
    public String toString() {
        return "MobileFoodFacility{" +
                "'cnn'='" + cnn +'\'' +
                ", 'objectid'='" + objectid +'\'' +
                ", 'x'='" + x +'\'' +
                ", 'y'='" + y +'\'' +
                ", 'latitude'='" + latitude +'\'' +
                ", 'status'='" + status + '\'' +
                ", 'permit'='" + permit + '\'' +
                ", 'block'='" + block + '\'' +
                ", 'facilitytype'='" + facilitytype + '\'' +
                ", 'blocklot'='" + blocklot + '\'' +
                ", 'locationdescription'='" + locationdescription + '\'' +
                ", 'priorpermit'='" + priorpermit + '\'' +
                ", 'schedule'='" + schedule + '\'' +
                ", 'address'='" + address + '\'' +
                ", 'applicant'='" + applicant + '\'' +
                ", 'lot'='" + lot + '\'' +
                ", 'received'='" + received +'\'' +
                ", 'noisent'='" + noisent +'\'' +
                ", 'expirationdate'='" + expirationdate +'\'' +
                ", 'approved'='" + approved +'\'' +
                ", 'location'=" + location +'\'' +
                ", 'fooditems'='" + fooditems +'\'' +
                '}';
    }
}

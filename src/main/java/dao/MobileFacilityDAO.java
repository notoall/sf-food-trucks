package dao;

import com.fasterxml.jackson.databind.ObjectMapper;
import model.MobileFoodFacility;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URL;
import java.util.Arrays;
import java.util.List;


/**
 * Created by Joshua on 1/18/2015.
 */
public class MobileFacilityDAO {
    private static final Logger logger = LogManager.getLogger(MobileFacilityDAO.class);

    private static final ObjectMapper MAPPER = new ObjectMapper();
    private static final String BASE_API_URL = "http://data.sfgov.org/resource/rqzj-sfat.json?";
    private static final String WITHIN_QUERY = "$where=within_box(location,xmin,ymin,xmax,ymax)";

    public List<MobileFoodFacility> getAll() {
        try {
            return Arrays.asList(MAPPER.readValue(new URL(BASE_API_URL), MobileFoodFacility[].class));
        } catch (Exception e) {
            logger.error("Failed to fetch all facilities", e);
        }

        return null;
    }

    public List<MobileFoodFacility> getAllWithinBounds(double xmin, double ymin, double xmax, double ymax) {
        try {
            URL url = new URL(generateUrlWithBounds(xmin, ymin, xmax, ymax));
            logger.info("Accessing resource: " + url);
            return Arrays.asList(MAPPER.readValue(url, MobileFoodFacility[].class));
        } catch (Exception e) {
            logger.error("Failed to fetch facilities within bounds", e);
        }

        return null;
    }

    private String generateUrlWithBounds(double xmin, double ymin, double xmax, double ymax) {
        return BASE_API_URL + WITHIN_QUERY.replace("xmin", String.valueOf(xmin))
                .replace("ymin", String.valueOf(ymin))
                .replace("xmax", String.valueOf(xmax))
                .replace("ymax", String.valueOf(ymax));
    }

}

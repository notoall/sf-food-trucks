package controller;

import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.server.ResourceConfig;
/**
 * Created by Joshua on 1/18/2015.
 */
public class Application extends ResourceConfig {

    public Application(){
        packages("controller").register(JacksonFeature.class);
    }
}

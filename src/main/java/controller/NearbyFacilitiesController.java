package controller;

import dao.MobileFacilityDAO;
import model.MobileFoodFacility;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * Created by Joshua on 1/12/2015.
 */

@Path("/facilities")
public class NearbyFacilitiesController {
    private static final Logger logger = LogManager.getLogger(NearbyFacilitiesController.class);
    private static final MobileFacilityDAO dao = new MobileFacilityDAO();

    /**
     * Given the bounds of a map, this method returns the list of all food trucks or carts int he area.
     *
     * @param xmin the minimum x coordinate
     * @param ymin the minimum y coordinate
     * @param xmax the maximum x coordinate
     * @param ymax the maximum y coordinate
     * @return list of all facilities within map bounds
     */
    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public List<MobileFoodFacility> getFacilities( @QueryParam("xmin")double xmin,
                                                   @QueryParam("ymin")double ymin,
                                                   @QueryParam("xmax")double xmax,
                                                   @QueryParam("ymax")double ymax) {
        logger.info("Bounds: min(" + xmin + ", " + ymin+") max("+xmax+", "+ymax+")");

        return dao.getAllWithinBounds(xmin,ymin,xmax, ymax);
    }
}

package deserializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Joshua on 1/12/2015.
 *
 * Used to deserialize the dates with non-uniform spaces
 */
public class RecievedDateDeserializer extends JsonDeserializer<Date>{

    private static final SimpleDateFormat FORMAT = new SimpleDateFormat("MMM dd yyyy HH:mmaa");

    @Override
    public Date deserialize(JsonParser jsonparser, DeserializationContext deserializationcontext) throws IOException, JsonProcessingException {
        String date = jsonparser.getText();
        if(date!=null && !date.isEmpty()) {
            try {
                return FORMAT.parse(date.replaceAll("\\s+"," "));
            } catch (ParseException e) {
                throw new RuntimeException(e);
            }
        }

        return null;

    }

}

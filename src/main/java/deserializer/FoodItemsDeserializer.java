package deserializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Joshua on 1/18/2015.
 */
public class FoodItemsDeserializer extends JsonDeserializer<Set<String>> {

    @Override
    public Set<String> deserialize(JsonParser jsonparser, DeserializationContext deserializationcontext) throws IOException {
        String foodItems = jsonparser.getText();
        if(foodItems!=null && !foodItems.isEmpty()) {
            return new HashSet<String>(Arrays.asList(foodItems.trim().toLowerCase().split("\\s*:\\s*")));
        }

        return null;

    }

}
